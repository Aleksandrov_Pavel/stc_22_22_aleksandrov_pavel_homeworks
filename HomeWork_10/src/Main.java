import java.util.HashMap;
import java.util.Map;

public class Main {
    public static void main(String[] args) {
        String string = "Hello Hello bye Hello Into";
        String[] words = string.split(" ");
        Map<String, Integer> map = new HashMap<>();

        for (String x : words) {
            int newValue = map.getOrDefault(x, 0) + 1;
            map.put(x, newValue);
        }

        Map.Entry<String, Integer> maxEntry = null;
        for (Map.Entry<String, Integer> entry : map.entrySet()) {
            if (maxEntry == null || entry.getValue() > maxEntry.getValue()) {
                maxEntry = entry;
            }
        }

        System.out.println(maxEntry);

    }
}