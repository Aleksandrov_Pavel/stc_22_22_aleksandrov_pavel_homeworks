package decision;

import java.util.Arrays;

public class Main {
    public static void main(String[] args) {
        // Создаём обект "square" класса "Square"
        Square square = new Square(12, 5, -1);
        // Создаём обект "rectagle" класса "Rectagle"
        Rectagle rectagle = new Rectagle(15, 5, 11, 44);
        // Создаём обект "circle" класса "Circle"
        Circle circle = new Circle(8.5, -5, -22);
        // Создаём обект "ellipse" класса "Ellipse"
        Ellipse ellipse = new Ellipse(19.5, 9, 0, 8);
        // Создаём массив класса Figure
        Figure[] shapes = new Figure[] {square, rectagle, circle, ellipse};
        // запускаем метод completeAll()
        completeAll(shapes);
    }
    public static void completeAll(Figure[] shapes) {
        for (int i = 0; i < shapes.length; i++) {
            String figure = "";
            switch (i) {
                case 0:
                    figure = "Квадрат";
                    break;
                case 1:
                    figure = "Прямоугольник";
                    break;
                case 2:
                    figure = "Круг";
                    break;
                case 3:
                    figure = "Эллипс";
                    break;
            }
            System.out.println("Фигура: " + figure + ", Площадь: " + shapes[i].aveaFigure() + ", Периметр: "
                    + shapes[i].perimeterFigure() + shapes[i].toString());
        }
    }
}








