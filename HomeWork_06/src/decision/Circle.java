package decision;

public class Circle extends Figure {
    // Создаём переменную "радиус круга"
    double radius;

    // Создаём конструктор
    public Circle(double radius, int x, int y) {
        super(x,y);
        this.radius = radius;
    }

    // Метод выщитывания площади
    public double aveaFigure() {
        return Math.PI * radius * radius;
    }

    // Метод выщитывания периметра
    public double perimeterFigure() {
        return 2 * Math.PI * radius;
    }
}