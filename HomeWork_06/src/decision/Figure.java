package decision;

public abstract class Figure {
    private int x = 0;
    private int y = 0;

    // Объявление метода расчёта площади фигуры
    public abstract double aveaFigure();

    // Объявление метода расчёта периметра фигур
    public abstract double perimeterFigure();

    // Создаём конструктор
    public Figure(int x, int y) {
        this.x = x;
        this.y = y;
    }

    // Создаём метод перемещения фигуры по заданным координатам

    @Override
    public String toString() {
        return ", Расположение: {" +
                "x=" + x +
                ", y=" + y +
                '}';
    }
}


