package decision;

public class Square extends Figure {

    // Создаём переменную "сторона квадрата"
    int side;

    // Создаём конструктор
    public Square(int side, int x, int y) {
        super(x,y);
        this.side = side;
    }

    // Метод выщитывания площади
    public double aveaFigure() {
        return side * side;
    }

    // Метод выщитывания периметра
    public double perimeterFigure() {
        return 4 * side;
    }
}