package decision;

public class Ellipse extends Circle {

    // Создаём переменную "Большая полуось эллипса"
    double radiusMajor;


    public Ellipse(double largeSemiAxisEllipse, double smallHalfAxisEllipse, int x, int y) {
        super(smallHalfAxisEllipse, x, y);
        radiusMajor = largeSemiAxisEllipse;
    }

    // Метод выщитывания площади
    public double aveaFigure() {
        return Math.PI * radiusMajor * radius;
    }

    // Метод выщитывания периметра
    public double perimeterFigure() {
        return +4 * (Math.PI * radiusMajor * radius + (radiusMajor - radius) * (radiusMajor - radius)) / (radiusMajor + radius);
    }

}