package decision;

public class Rectagle extends Square {

    // Создаём переменную "ширина прямоугольника"
    private int widthRectagle;

    public Rectagle(int widthRectagle, int longRectagle, int x, int y) {
        super(longRectagle, x, y);
        this.widthRectagle = widthRectagle;
    }

    // Метод выщитывания площади
    public double aveaFigure() {
        return widthRectagle * side;
    }

    // Метод выщитывания периметра
    public double perimeterFigure() {
        return 2 * (widthRectagle + side);
    }
}