package ru.pva88.cars.app;

import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import ru.pva88.cars.config.AppConfig;
import ru.pva88.cars.models.Cars;
import ru.pva88.cars.services.FindCarsService;
import ru.pva88.cars.services.SaveCarsService;

import java.util.Scanner;


public class Main {
    public static void main(String[] args) {

        Scanner scanner = new Scanner(System.in);
        String action = args[0];

        ApplicationContext context = new AnnotationConfigApplicationContext(AppConfig.class);
        FindCarsService findCarsService = context.getBean(FindCarsService.class);
        SaveCarsService saveCarsService = context.getBean(SaveCarsService.class);



        if (action.equals("read")) {
            for (Cars car : findCarsService.getAllCars()) {
                System.out.println(car);
            }

        } else if (action.equals("write")) {
            while (true) {
                System.out.println("Введите модель автомобиля");
                String model = scanner.nextLine();
                System.out.println("Введите цвет автомобиля");
                String colour = scanner.nextLine();
                System.out.println("Введите номер автомобиля");
                String carNumber = scanner.nextLine();
                System.out.println("Введите фамилию владельца");
                String ownerCar = scanner.nextLine();

                saveCarsService.signUp(model, colour, carNumber, ownerCar);

                System.out.println("Продолжаем вводить данные в таблицу? Y/N");
                String field = scanner.nextLine();
                if (field.equalsIgnoreCase("N")) {
                    break;
                }

            }

        }


    }
}