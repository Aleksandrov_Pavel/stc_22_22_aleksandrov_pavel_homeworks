package ru.pva88.cars.services.impl;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import ru.pva88.cars.models.Cars;
import ru.pva88.cars.repositories.SaveCarsRepository;
import ru.pva88.cars.services.SaveCarsService;

@Service
@RequiredArgsConstructor
public class SaveCarsServiceImpl implements SaveCarsService {

    private final SaveCarsRepository saveCarsRepository;

    @Override
    public void signUp(String model, String colour, String carNumber, String ownerCar) {
        Cars cars = Cars.builder()
                .model(model)
                .colour(colour)
                .carNumber(carNumber)
                .ownerCar(ownerCar)
                .build();
        saveCarsRepository.save(cars);
    }
}
