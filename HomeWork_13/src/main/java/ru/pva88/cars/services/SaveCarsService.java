package ru.pva88.cars.services;

public interface SaveCarsService {
    void signUp(String model, String colour, String carNumber, String ownerCar);
}
