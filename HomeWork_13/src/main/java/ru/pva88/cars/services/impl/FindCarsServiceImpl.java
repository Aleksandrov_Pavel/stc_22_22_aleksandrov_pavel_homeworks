package ru.pva88.cars.services.impl;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import ru.pva88.cars.models.Cars;
import ru.pva88.cars.repositories.FindCarsRepository;
import ru.pva88.cars.services.FindCarsService;

import java.util.List;

@Service
@RequiredArgsConstructor
public class FindCarsServiceImpl implements FindCarsService {

    private final FindCarsRepository findCarsRepository;

    @Override
    public List<Cars> getAllCars() {
        return findCarsRepository.findAll();
    }


}
