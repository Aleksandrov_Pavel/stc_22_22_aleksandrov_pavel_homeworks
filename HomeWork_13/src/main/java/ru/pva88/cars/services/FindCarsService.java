package ru.pva88.cars.services;

import ru.pva88.cars.models.Cars;

import java.util.List;

public interface FindCarsService {

    List<Cars> getAllCars();
}
