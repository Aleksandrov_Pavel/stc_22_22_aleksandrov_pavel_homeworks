package ru.pva88.cars.repositories;

import ru.pva88.cars.models.Cars;

import java.util.List;

public interface FindCarsRepository {

    List<Cars> findAll();
}
