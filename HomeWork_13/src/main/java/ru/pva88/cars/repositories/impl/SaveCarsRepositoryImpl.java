package ru.pva88.cars.repositories.impl;

import lombok.RequiredArgsConstructor;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;
import ru.pva88.cars.models.Cars;
import ru.pva88.cars.repositories.SaveCarsRepository;

@Repository
@RequiredArgsConstructor
public class SaveCarsRepositoryImpl implements SaveCarsRepository {

    public final JdbcTemplate jdbcTemplate;


    //language=SQL
    public final static String SQL_INSERT =
            "insert into garage.cars(model, colour, car_number, owner_car) values (?, ?, ?, ?)";


    @Override
    public void save(Cars cars) {
        jdbcTemplate.update(SQL_INSERT, cars.getModel(), cars.getColour(),
                cars.getCarNumber(), cars.getOwnerCar());
    }
}
