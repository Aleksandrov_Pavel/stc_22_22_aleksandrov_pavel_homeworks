package ru.pva88.cars.repositories;

import ru.pva88.cars.models.Cars;

public interface SaveCarsRepository {
    void save(Cars cars);
}
