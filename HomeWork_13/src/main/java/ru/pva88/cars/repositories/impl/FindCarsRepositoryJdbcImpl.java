package ru.pva88.cars.repositories.impl;

import lombok.RequiredArgsConstructor;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;
import ru.pva88.cars.models.Cars;
import ru.pva88.cars.repositories.FindCarsRepository;

import java.util.List;

@Repository
@RequiredArgsConstructor
public class FindCarsRepositoryJdbcImpl implements FindCarsRepository {

    private final JdbcTemplate jdbcTemplate;

    //language=SQL
    private final static String SQL_Select_All = "select * from garage.cars";

    @Override
    public List<Cars> findAll() {
        return jdbcTemplate.query(SQL_Select_All, new BeanPropertyRowMapper<>(Cars.class));
    }
}
