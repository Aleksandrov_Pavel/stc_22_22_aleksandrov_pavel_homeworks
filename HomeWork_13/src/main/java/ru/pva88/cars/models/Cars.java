package ru.pva88.cars.models;

import lombok.*;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@ToString
@EqualsAndHashCode
@Builder
public class Cars {

    private Long idCars;
    private String model;
    private String colour;
    private String carNumber;
    private String ownerCar;
}
