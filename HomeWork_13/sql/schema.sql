create schema garage;

create table garage.cars
(
    id_cars    bigserial primary key,
    model      varchar not null,
    colour     varchar not null,
    car_number varchar not null,
    owner_car  varchar not null
);

 -- drop schema if exists garage cascade;