import java.util.*;

public class Main {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        System.out.println("Создаём массив!");
        System.out.println("Введите длину массива:");
        int arrayLength = scanner.nextInt();

        int[] arrayResult = array(arrayLength);

        /** ЗАДАНИЕ № 1:
         *  Написать функцию, возвращающую сумму чисел в каком-либо интервале, если интервал
         *  задан неверно (значение левой границы больше, чем правой) - вернуть -1
         */

        System.out.println("Задаём интервал для подсчёта суммы чисел");
        System.out.println("Введите число левого значения интервала");
        int leftInterval = scanner.nextInt();
        System.out.println("Введите число правого значения интервала");
        int rightInterval = scanner.nextInt();

        System.out.println("Результат: " + calcSumOfArrayRange(arrayResult, leftInterval, rightInterval));


        /** ЗАДАНИЕ № 2:
         *  Написать процедуру, которая для заданного массива выводит все его четные элементы
         */

        EvenNumbers(arrayResult);


        /** ЗАДАНИЕ № 3: Реализовать функцию toInt()
         */

        System.out.println("Создаём второй массив!");
        System.out.println("Введите длину массива:");
        int arrayTwoLength = scanner.nextInt();

        int[] arrayTwoResult = array(arrayTwoLength);
        System.out.println("Результат реализации функции toInt(): " + toInt(arrayTwoResult));
    }
    public static int[] array(int length) {
        Scanner scanner = new Scanner(System.in);
        int[] result = new int[length];
        for (int i = 0; i < result.length; i++) {
            System.out.println("Введите число:");
            result[i] = scanner.nextInt();
        }
        return result;
    }
    // Задание № 1
    public static int calcSumOfArrayRange(int[] a, int from, int to) {
        int sum = 0;
        int rez = 0;
        if (from > to || from < 0 || to > a.length) {
            rez = -1;
        } else {
            for (int i = from; i <= to; i++) {
                sum += a[i];
                rez = sum;
            }
        }
        return rez;
    }
        // Задание № 2
    public static void EvenNumbers(int[] a) {
        System.out.print("Выводим чётные числа: ");
        int flag = 0;
        for (int i = 0; i < a.length; i++) {
            if (a[i] % 2 == 0) {
                System.out.print(a[i] + " ");
                flag += 1;
            }
        }
        if (flag == 0) {
            System.out.println("Чётных чисел нет!");
        }
        System.out.println();
    }
    // Задание № 3
    public static String toInt(int[] a) {
        String[] arrayTwo = new String[a.length];
        for(int i = 0; i < a.length; i++){
            arrayTwo[i] = String.valueOf(a[i]);
        }
        String result = "";
        for (int i = 0; i < arrayTwo.length; i++) {
            result += arrayTwo[i];
        }
        return result;
    }
}

