package HomeWork;

import java.util.Scanner;

public class Array {

    private int[] arrays;
    private int from;
    private int to;

    public int[] getArrays() {
        return arrays;
    }

    public int getFrom() {
        return from;
    }

    public int getTo() {
        return to;
    }

    public void creatingAnArray() {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Создаём массив");
        System.out.print("Введите длинну массива: ");

        int[] arrays = new int[scanner.nextInt()];

        for (int i = 0; i < arrays.length; i++) {
            System.out.print("Введите число " + i + " индекса массива: ");
            arrays[i] = scanner.nextInt();
        }

        System.out.print("Введите левый индекс массива для поиска в массиве: ");
        int from = scanner.nextInt();
        System.out.print("Введите правый индекс массива для поиска в массиве: ");
        int to = scanner.nextInt();

        this.arrays = arrays;
        this.from = from;
        this.to = to;

    }
}
