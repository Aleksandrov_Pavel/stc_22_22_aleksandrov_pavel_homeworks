package HomeWork;

public class Main {
    public static void main(String[] args) {

        Array creatingAnArray = new Array();
        // Создаём массив чисел
        creatingAnArray.creatingAnArray();

        // Лямбда-выражения. Вычисление суммы заданных элементов массива
        ArrayTask sumOfElement = (array, x, y) -> {
            int result = 0;

            for (int i = x; i <= y; i++) {
                result += array[i];
            }
            return result;
        };

        // Лямбда-выражения. Вычисление суммы цифр наибольшего элемента массива
        ArrayTask sumOfElementMaxDigits = (array, x, y) -> {
            int result = 0;
            int maxNumber = 0;

            for (int i = x; i <= y; i++) {
                if (array[i] > maxNumber) {
                    maxNumber = array[i];
                }
            }
            while (maxNumber != 0) {
                result += (maxNumber % 10);
                maxNumber /= 10;
            }
            return result;
        };

        print();
        System.out.println("Вычисление суммы заданных элементов массива");
        ArraysTasksResolver.resolveTask(creatingAnArray.getArrays(), sumOfElement, creatingAnArray.getFrom(), creatingAnArray.getTo());
        print();
        System.out.println("Вычесление суммы цифр наибольшего элемента массива");
        ArraysTasksResolver.resolveTask(creatingAnArray.getArrays(), sumOfElementMaxDigits, creatingAnArray.getFrom(), creatingAnArray.getTo());
    }

    public static void print() {
        System.out.println();
        System.out.println();
    }
}