package HomeWork;

import java.util.Arrays;

public class ArraysTasksResolver {

    public static void resolveTask(int[] array, ArrayTask task, int from, int to) {
        // Выводим исходный массив
        System.out.println("Исходный массив: " + Arrays.toString(array));
        // Выводим значение "from"
        System.out.println("Левый индекс массива для поиска: " + from);
        // Выводим значение "to"
        System.out.println("Правый индекс массива для поиска: " + to);
        // Выводим результат
        System.out.println("Результат: " + task.resolve(array, from, to));
    }
}

