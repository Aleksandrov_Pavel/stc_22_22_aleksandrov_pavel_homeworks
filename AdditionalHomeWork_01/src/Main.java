import java.util.Scanner;

public class Main {
    public static void main(String[] args) {

        int number;
        int evel = 0;
        int odd = 1;
        int flag = 0;

        Scanner scanner = new Scanner(System.in);

        System.out.println("Введите число");

        number = scanner.nextInt();

        while(number != -1) {
            if(number % 2 == 0) {
                evel += 1;
            } else {
                odd *= number;
                flag += 1;
            }
            System.out.println("Введите число");
            number = scanner.nextInt();
        }
        if (flag == 0){
            odd = 0;
        }

        System.out.println("Произведение всех нечетных чисел: " + odd);
        System.out.println("Количество четных чисел: " + evel);
    }
}
