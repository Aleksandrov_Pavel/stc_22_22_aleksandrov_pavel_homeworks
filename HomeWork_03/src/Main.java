import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        System.out.println("Введите длинну массива:");

        int length = scanner.nextInt();

        int[] array = new int[length];
        int flag = 0;

        for (int i = 0; i < array.length; i++) {
            System.out.println("Введите число:");
            array[i] = scanner.nextInt();
        }

        System.out.print("Локальный минимум: ");

        for (int i = 1; i < array.length; i++) {
            if (i == 1 && array[1] > array[0]) {
                System.out.print(array[0] + " ");
                flag += 1;
            }
            if (i >= 2 && array[i] > array[i - 1] && array[i - 1] < array [i - 2]) {
                System.out.print(array[i - 1] + " ");
                flag += 1;
            }
            if (i == array.length - 1 && array[length - 1] < array[length - 2]) {
                System.out.println(array[length - 1]);
                flag += 1;
            }
        }
        System.out.println("");

        if (flag > 0) {
            System.out.println("Колличество локальных минимумов: " + flag);
        } else {
            System.out.println("Отсутствуют");
        }
    }
}