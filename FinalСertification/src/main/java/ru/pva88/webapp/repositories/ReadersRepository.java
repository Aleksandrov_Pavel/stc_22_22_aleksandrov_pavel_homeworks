package ru.pva88.webapp.repositories;


import org.springframework.data.jpa.repository.JpaRepository;
import ru.pva88.webapp.models.Department;
import ru.pva88.webapp.models.Reader;

import java.util.List;
import java.util.Optional;


public interface ReadersRepository extends JpaRepository<Reader, Long> {

    // лист отмеченных
    List<Reader> findAllByStateNot(Reader.State state);

    List<Reader> findAllByDepartmentNotContainsAndState(Department department, Reader.State state);

    List<Reader> findAllByDepartmentContains(Department department);

    Optional<Reader> findByEmail(String email);
}

