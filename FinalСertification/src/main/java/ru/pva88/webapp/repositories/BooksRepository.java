package ru.pva88.webapp.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import ru.pva88.webapp.models.Department;
import ru.pva88.webapp.models.Book;

import java.util.List;

@Repository
public interface BooksRepository extends JpaRepository<Book, Long> {

    List<Book> findAllByStateNot(Book.State state);

    List<Book> findAllByDepartmentNullAndState(Book.State state);

    List<Book> findAllByDepartment(Department Department);
}
