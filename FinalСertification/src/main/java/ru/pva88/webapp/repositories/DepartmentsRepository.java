package ru.pva88.webapp.repositories;


import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import ru.pva88.webapp.models.Department;

import java.util.List;

@Repository
public interface DepartmentsRepository extends JpaRepository<Department, Long> {

    List<Department> findAllByStateNot(Department.State state);


}

