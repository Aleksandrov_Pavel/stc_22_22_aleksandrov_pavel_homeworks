package ru.pva88.webapp.security.details;

import lombok.RequiredArgsConstructor;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import ru.pva88.webapp.models.Reader;
import ru.pva88.webapp.repositories.ReadersRepository;


@RequiredArgsConstructor
@Service
public class CustomReaderDetailsService implements UserDetailsService {

    private final ReadersRepository readersRepository;

    @Override
    public UserDetails loadUserByUsername(String email) throws UsernameNotFoundException {

        Reader reader = readersRepository.findByEmail(email).orElseThrow(() -> new UsernameNotFoundException("User not found"));
        return new CustomReaderDetails(reader);
    }
}
