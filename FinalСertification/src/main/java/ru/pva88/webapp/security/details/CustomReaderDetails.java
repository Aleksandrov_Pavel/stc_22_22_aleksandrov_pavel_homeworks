package ru.pva88.webapp.security.details;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import ru.pva88.webapp.models.Reader;

import java.util.Collection;
import java.util.Collections;

public class CustomReaderDetails implements UserDetails {

    private Reader reader;

    public Reader getReader() {
        return reader;
    }

    public CustomReaderDetails(Reader reader) {
        this.reader = reader;
    }

    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        String role = reader.getRole().toString();
        return Collections.singleton(new SimpleGrantedAuthority(role));
    }


    @Override
    public String getPassword() {
        return reader.getPassword();
    }

    @Override
    public String getUsername() {
        return reader.getEmail();
    }

    @Override
    public boolean isAccountNonExpired() {
        return true;
    }

    @Override
    public boolean isAccountNonLocked() {
        return !reader.getState().equals(Reader.State.DELETED);
    }

    @Override
    public boolean isCredentialsNonExpired() {
        return true;
    }

    @Override
    public boolean isEnabled() {
        return reader.getState().equals(Reader.State.CONFIRMED);
    }

}
