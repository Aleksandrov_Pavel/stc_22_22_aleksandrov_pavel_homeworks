package ru.pva88.webapp.controllers;


import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import ru.pva88.webapp.dto.ReadersForm;
import ru.pva88.webapp.services.ReadersService;


@Controller
@RequestMapping(value = "/readers")
@RequiredArgsConstructor
public class ReadersController {

    private final ReadersService readersService;

    @GetMapping
    public String getReadersPage(@RequestParam(value = "orderBy", required = false) String orderBy,
                                 @RequestParam(value = "dir", required = false) String direction, Model model) {
        model.addAttribute("readersList", readersService.getAllReaders());
        return "readers_page";
    }

    @PostMapping
    public String addReaders(ReadersForm readers) {
        readersService.addReaders(readers);
        return "redirect:/readers";
    }

    @GetMapping("/{reader-id}")
    public String getReadersPage(@PathVariable("reader-id") Long id, Model model) {
        model.addAttribute("reader", readersService.getReaders(id));
        return "reader_page";
    }

    @PostMapping("/{reader-id}/update")
    public String updateReaders(@PathVariable("reader-id") Long readersId, ReadersForm readers) {
        readersService.updateReaders(readersId, readers);
        return "redirect:/readers/" + readersId;
    }

    @GetMapping("/{reader-id}/delete")
    public String deleteReaders(@PathVariable("reader-id") Long readersId) {
        readersService.deleteReaders(readersId);
        return "redirect:/readers";
    }



}
