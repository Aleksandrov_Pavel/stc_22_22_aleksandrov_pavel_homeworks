package ru.pva88.webapp.controllers;


import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import ru.pva88.webapp.dto.DepartmentsForm;
import ru.pva88.webapp.services.DepartmentsService;


@Controller
@RequestMapping(value = "/departments")
@RequiredArgsConstructor
public class DepartmentsController {

    private final DepartmentsService departmentsService;

    @GetMapping
    public String getDepartmentsPage(Model model) {
        model.addAttribute("departments", departmentsService.getAllDepartments());
        return "departments_page";
    }

    @PostMapping
    public String addDepartments(DepartmentsForm departments) {
        departmentsService.addDepartments(departments);
        return "redirect:/departments";
    }

    @GetMapping("/{department-id}")
    public String getDepartmentsPage(@PathVariable("department-id") Long departmentId, Model model) {
        model.addAttribute("department", departmentsService.getDepartment(departmentId));
        model.addAttribute("notInDepartmentBooks", departmentsService.getNotInDepartmentBooks());
        model.addAttribute("inDepartmentBooks", departmentsService.getInDepartmentBooks(departmentId));
        model.addAttribute("notInDepartmentReaders", departmentsService.getNotInDepartmentReader(departmentId));
        model.addAttribute("inDepartmentReaders", departmentsService.getInDepartmentReader(departmentId));
        return "department_page";
    }

    @PostMapping("/{department-id}/update")
    public String updateDepartments(@PathVariable("department-id") Long departmentId, DepartmentsForm department) {
        departmentsService.updateDepartments(departmentId, department);
        return "redirect:/departments/" + departmentId;
    }

    @GetMapping("/{department-id}/delete")
    public String deleteDepartments(@PathVariable("department-id") Long departmentId) {
        departmentsService.deleteDepartments(departmentId);
        return "redirect:/departments";
    }

    @PostMapping("/{department-id}/readers")
    public String addReaderToDepartment(@PathVariable("department-id") Long departmentId,
                                         @RequestParam("reader-id") Long readerId) {
        departmentsService.addReaderToDepartment(departmentId, readerId);
        return "redirect:/departments/" + departmentId;
    }

    @PostMapping("/{department-id}/books")
    public String addBookToDepartment(@PathVariable("department-id") Long departmentId,
                                        @RequestParam("book-id") Long bookId) {
        departmentsService.addBookToDepartment(departmentId, bookId);
        return "redirect:/departments/" + departmentId;
    }

    @GetMapping("/{department-id}/remove")
    public String removeBook(@PathVariable("department-id") Long departmentId,
                               @RequestParam("book-id") Long bookId) {
        departmentsService.removeBook(bookId);
        return "redirect:/departments/" + departmentId;
    }

}
