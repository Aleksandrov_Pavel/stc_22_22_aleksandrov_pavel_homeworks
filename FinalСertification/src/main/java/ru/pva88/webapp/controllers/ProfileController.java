package ru.pva88.webapp.controllers;

import lombok.RequiredArgsConstructor;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import ru.pva88.webapp.security.details.CustomReaderDetails;
import ru.pva88.webapp.services.ProfileService;


@RequiredArgsConstructor
@Controller
public class ProfileController {

    final private ProfileService profileService;

    @GetMapping("/")
    public String getRoot() {
        return "redirect:/profile";
    }

    @GetMapping("/profile")
    public String getProfile(@AuthenticationPrincipal CustomReaderDetails readerDetails, Model model) {
        model.addAttribute("reader", profileService.getCurrent(readerDetails));
        return "profile_page";
    }
}
