package ru.pva88.webapp.controllers;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import ru.pva88.webapp.dto.BooksForm;
import ru.pva88.webapp.services.BooksService;

@RequiredArgsConstructor
@RequestMapping(value = "/books")
@Controller
public class BooksController {

    private final BooksService booksService;


    @GetMapping
    public String getBooksPage(Model model) {
        model.addAttribute("books", booksService.getAllBooks());
        return "books_page";
    }

    @PostMapping
    public String addBooks(BooksForm books) {
        booksService.addBooks(books);
        return "redirect:/books";
    }

    @GetMapping("/{book-id}")
    public String getBookPage(@PathVariable("book-id") Long bookId, Model model) {
        model.addAttribute("book", booksService.getBook(bookId));
        return "book_page";
    }

    @PostMapping("/{book-id}/update")
    public String updateBooks(@PathVariable("book-id") Long bookId, BooksForm books) {
        booksService.updateBooks(bookId, books);
        return "redirect:/books/" + bookId;
    }

    @GetMapping("/{book-id}/delete")
    public String deleteBooks(@PathVariable("book-id") Long bookId) {
        booksService.deleteBooks(bookId);
        return "redirect:/books";
    }
}

