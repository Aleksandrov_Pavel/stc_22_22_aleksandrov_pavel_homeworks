package ru.pva88.webapp.models;

import lombok.*;

import javax.persistence.*;
import java.time.LocalTime;


@Data
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode(exclude = "department")
@ToString(exclude = "department")
@Builder
@Entity
public class Book {

    public enum State {
        CONFIRMED, DELETED
    }

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String name;
    private String summary;


    @ManyToOne
    @JoinColumn(name = "department_id")
    private Department department;

    @Enumerated(value = EnumType.STRING)
    private State state;
}
