package ru.pva88.webapp.models;

import lombok.*;

import javax.persistence.*;
import java.time.LocalDate;
import java.util.Set;




@Data
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode(exclude = {"book", "readers"})
@ToString
@Builder
@Entity
public class Department {

    public enum State {
        CONFIRMED, DELETED
    }

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String title;
    private String description;

    @Column(name = "start_work")
    private LocalDate start;

    private LocalDate finish;

    @ManyToMany(mappedBy = "department", fetch = FetchType.EAGER)
    private Set<Reader> readers;

    @OneToMany(mappedBy = "department", fetch = FetchType.EAGER)
    private Set<Book> books;

    @Enumerated(value = EnumType.STRING)
    private State state;
}
