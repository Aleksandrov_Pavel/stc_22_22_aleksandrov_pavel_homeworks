package ru.pva88.webapp.services.impl;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import ru.pva88.webapp.dto.DepartmentsForm;
import ru.pva88.webapp.models.Department;
import ru.pva88.webapp.models.Book;
import ru.pva88.webapp.models.Reader;
import ru.pva88.webapp.repositories.DepartmentsRepository;
import ru.pva88.webapp.repositories.BooksRepository;
import ru.pva88.webapp.repositories.ReadersRepository;
import ru.pva88.webapp.services.DepartmentsService;

import java.time.LocalDate;
import java.util.List;

@Service
@RequiredArgsConstructor
public class DepartmentsServiceImpl implements DepartmentsService {

    private final DepartmentsRepository departmentsRepository;

    private final BooksRepository booksRepository;

    private final ReadersRepository readersRepository;

    @Override
    public List<Department> getAllDepartments() {
        return departmentsRepository.findAllByStateNot(Department.State.DELETED);
    }

    @Override
    public Department getDepartment(Long departmentId) {
        return departmentsRepository.findById(departmentId).orElseThrow();
    }


    @Override
    public void deleteDepartments(Long departmentId) {
        Department departmentForDelete = departmentsRepository.findById(departmentId).orElseThrow();
        departmentForDelete.setState(Department.State.DELETED);
        departmentsRepository.save(departmentForDelete);
    }

    @Override
    public void addDepartments(DepartmentsForm department) {
        Department newDepartment = Department.builder()
                .title(department.getTitle())
                .description(department.getDescription())
                .start(LocalDate.EPOCH)
                .finish(LocalDate.EPOCH)
                .state(Department.State.CONFIRMED)
                .build();
        departmentsRepository.save(newDepartment);
    }

    @Override
    public void updateDepartments(Long departmentId, DepartmentsForm departments) {
        Department departmentForUpdate = departmentsRepository.findById(departmentId).orElseThrow();
        departmentForUpdate.setTitle(departments.getTitle());
        departmentForUpdate.setDescription(departments.getDescription());
        departmentForUpdate.setStart(departments.getStart());
        departmentForUpdate.setFinish(departments.getFinish());
        departmentsRepository.save(departmentForUpdate);
    }


    @Override
    public List<Book> getNotInDepartmentBooks() {
        return booksRepository.findAllByDepartmentNullAndState(Book.State.CONFIRMED);
    }

    @Override
    public List<Book> getInDepartmentBooks(Long departmentId) {
        Department department = departmentsRepository.findById(departmentId).orElseThrow();
        return booksRepository.findAllByDepartment(department);
    }


    @Override
    public void addBookToDepartment(Long departmentId, Long bookId) {
        Department department = departmentsRepository.findById(departmentId).orElseThrow();
        Book book = booksRepository.findById(bookId).orElseThrow();
        book.setDepartment(department);
        booksRepository.save(book);
    }

    @Override
    public void addReaderToDepartment(Long departmentId, Long readerId) {
        Department department = departmentsRepository.findById(departmentId).orElseThrow();
        Reader reader = readersRepository.findById(readerId).orElseThrow();
        reader.getDepartment().add(department);
        readersRepository.save(reader);
    }

    @Override
    public List<Reader> getNotInDepartmentReader(Long departmentId) {
        Department department = departmentsRepository.findById(departmentId).orElseThrow();
        return readersRepository.findAllByDepartmentNotContainsAndState(department, Reader.State.CONFIRMED);
    }

    @Override
    public List<Reader> getInDepartmentReader(Long departmentId) {
        Department department = departmentsRepository.findById(departmentId).orElseThrow();
        return readersRepository.findAllByDepartmentContains(department);
    }

    @Override
    public void removeBook(Long bookId) {
        Book book = booksRepository.findById(bookId).orElseThrow();
        book.setDepartment(null);
        booksRepository.save(book);
    }
}