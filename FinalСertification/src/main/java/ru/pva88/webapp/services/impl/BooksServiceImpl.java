package ru.pva88.webapp.services.impl;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import ru.pva88.webapp.dto.BooksForm;
import ru.pva88.webapp.models.Book;
import ru.pva88.webapp.repositories.BooksRepository;
import ru.pva88.webapp.services.BooksService;

import java.time.LocalTime;
import java.util.List;


@Service
@RequiredArgsConstructor
public class BooksServiceImpl implements BooksService {

    private final BooksRepository booksRepository;


    @Override
    public List<Book> getAllBooks() {
        return booksRepository.findAllByStateNot(Book.State.DELETED);
    }

    @Override
    public void addBooks(BooksForm book) {
        Book newBook = Book.builder()
                .name(book.getName())
                .summary(book.getSummary())
                .state(Book.State.CONFIRMED)
                .build();
        booksRepository.save(newBook);
    }

    @Override
    public Book getBook(Long bookId) {
        return booksRepository.findById(bookId).orElseThrow();
    }

    @Override
    public void updateBooks(Long bookId, BooksForm updateBook) {
        Book bookForUpdate = booksRepository.findById(bookId).orElseThrow();
        bookForUpdate.setName(updateBook.getName());
        bookForUpdate.setSummary(updateBook.getSummary());
        booksRepository.save(bookForUpdate);
    }

    @Override
    public void deleteBooks(Long bookIs) {
        Book booksForDeleted = booksRepository.findById(bookIs).orElseThrow();
        booksForDeleted.setState(Book.State.DELETED);
        booksForDeleted.setDepartment(null);
        booksRepository.save(booksForDeleted);
    }

}
