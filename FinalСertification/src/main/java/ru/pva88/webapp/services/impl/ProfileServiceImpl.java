package ru.pva88.webapp.services.impl;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import ru.pva88.webapp.security.details.CustomReaderDetails;
import ru.pva88.webapp.models.Reader;
import ru.pva88.webapp.repositories.ReadersRepository;
import ru.pva88.webapp.services.ProfileService;

@RequiredArgsConstructor
@Service
public class ProfileServiceImpl implements ProfileService {

    private final ReadersRepository readersRepository;

    @Override
    public Reader getCurrent(CustomReaderDetails readerDetails) {
        return readersRepository.findById(readerDetails.getReader().getId()).orElseThrow();
    }
}
