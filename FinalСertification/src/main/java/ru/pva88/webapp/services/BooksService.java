package ru.pva88.webapp.services;

import ru.pva88.webapp.dto.BooksForm;
import ru.pva88.webapp.models.Book;

import java.util.List;

public interface BooksService {

    List<Book> getAllBooks();

    void addBooks(BooksForm books);

    Book getBook(Long bookId);

    void updateBooks(Long bookId, BooksForm books);

    void deleteBooks(Long bookId);

}


