package ru.pva88.webapp.services;


import ru.pva88.webapp.models.Reader;
import ru.pva88.webapp.security.details.CustomReaderDetails;

public interface ProfileService {

    Reader getCurrent(CustomReaderDetails readerDetails);
}
