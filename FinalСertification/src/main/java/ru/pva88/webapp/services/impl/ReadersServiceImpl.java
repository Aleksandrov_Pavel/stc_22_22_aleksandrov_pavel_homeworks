package ru.pva88.webapp.services.impl;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import ru.pva88.webapp.dto.ReadersForm;
import ru.pva88.webapp.models.Reader;
import ru.pva88.webapp.repositories.ReadersRepository;
import ru.pva88.webapp.services.ReadersService;

import java.util.List;

@Service
@RequiredArgsConstructor
public class ReadersServiceImpl implements ReadersService {

    private final ReadersRepository readersRepository;

    @Override
    public List<Reader> getAllReaders() {
        return readersRepository.findAllByStateNot(Reader.State.DELETED);
    }

    @Override
    public void addReaders(ReadersForm reader) {
        Reader newReaders = Reader.builder()
                .email(reader.getEmail())
                .lastName(reader.getLastName())
                .firstName(reader.getFirstName())
                .state(Reader.State.CONFIRMED)
                .role(Reader.Role.USER)
                .build();
        readersRepository.save(newReaders);
    }

    @Override
    public Reader getReaders(Long id) {
        return  readersRepository.findById(id).orElseThrow();
    }

    @Override
    public void updateReaders(Long readersId, ReadersForm updateReaders) {
        Reader readersForUpdate = readersRepository.findById(readersId).orElseThrow();
        readersForUpdate.setFirstName(updateReaders.getFirstName());
        readersForUpdate.setLastName(updateReaders.getLastName());
        readersForUpdate.setAge(updateReaders.getAge());
        readersRepository.save(readersForUpdate);
    }

    @Override
    public void deleteReaders(Long readerIs) {
        Reader readersForDeleted = readersRepository.findById(readerIs).orElseThrow();
        readersForDeleted.setState(Reader.State.DELETED);
        readersRepository.save(readersForDeleted);
    }
}
