package ru.pva88.webapp.services;

import ru.pva88.webapp.dto.DepartmentsForm;
import ru.pva88.webapp.models.Department;
import ru.pva88.webapp.models.Book;
import ru.pva88.webapp.models.Reader;

import java.util.List;

public interface DepartmentsService {

    List<Department> getAllDepartments();

    void addDepartments(DepartmentsForm department);

    Department getDepartment(Long departmentId);

    void updateDepartments(Long departmentId, DepartmentsForm department);

    void deleteDepartments(Long departmentId);

    void addBookToDepartment(Long departmentId, Long bookId);

    List<Book> getNotInDepartmentBooks();

    List<Book> getInDepartmentBooks(Long departmentId);

    void addReaderToDepartment(Long departmentId, Long readerId);

    List<Reader> getNotInDepartmentReader(Long departmentId);

    List<Reader> getInDepartmentReader(Long departmentId);

    void removeBook(Long lessonId);
}