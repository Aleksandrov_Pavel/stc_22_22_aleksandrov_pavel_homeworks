package ru.pva88.webapp.services;

import ru.pva88.webapp.dto.ReadersForm;
import ru.pva88.webapp.models.Reader;

import java.util.List;

public interface ReadersService {

    List<Reader> getAllReaders();

    void addReaders(ReadersForm readers);

    Reader getReaders(Long id);

    void updateReaders(Long readersId, ReadersForm readers);

    void deleteReaders(Long readerId);
}
