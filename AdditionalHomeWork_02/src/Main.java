import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        System.out.println("Введите первое число:");
        int from = scanner.nextInt();

        System.out.println("Введите второе число:");
        int to = scanner.nextInt();

        if (from < to) {
            for (int a = from; a <= to; a++) {
                System.out.print(a + " ");
            }
        } else {
            for (int b = from; b >= to; b--) {
                System.out.print(b + " ");
            }
        }
    }
}