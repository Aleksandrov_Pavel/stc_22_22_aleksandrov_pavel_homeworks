public abstract class AbstractNumbersPrintTask implements Task {

    private int from;
    private int to;

    public AbstractNumbersPrintTask(int from, int to) {
        this.from = from;
        this.to = to;
    }

    public abstract void complete();


}
