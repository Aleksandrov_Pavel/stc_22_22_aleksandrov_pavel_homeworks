import java.util.Scanner;

public class Main {
    public static void main(String[] args) {

        System.out.println("Создаём массив");
        System.out.print("Введите длину массива: ");
        Scanner scanner = new Scanner(System.in);
        int lengthArray = scanner.nextInt();
        int[] array = new int[lengthArray];

        for (int i = 0; i < array.length; i++) {
            System.out.print("Введите число " + i + " массива: ");
            array[i] = scanner.nextInt();
        }
        int fromEvenNumber;
        int toEvenNumber;
        int fromOddNumber;
        int toOddNumber;

        println();

        System.out.println("Поиск положительных чисел в массиве");
        while (true) {
            System.out.print("Введите индекс с какоторого начнём поиск положительных чисел: ");
            int number = scanner.nextInt();
            if (number < 0 || number > array.length - 1) {
                System.out.println("Ввели несуществующий интекс. Повторите попытку.");
            } else {
                fromEvenNumber = number;
                break;
            }
        }

        while (true) {
            System.out.print("Введите индекс до какорого будем искать положительное число: ");
            int number = scanner.nextInt();
            if (number < 0 || number > array.length) {
                System.out.println("Ввели несуществующий интекс. Повторите попытку.");
            } else if (fromEvenNumber > number) {
                System.out.println("Вы ввели индекс меньше первого. Повторите попытку.");
            } else {
                toEvenNumber = number;
                break;
            }
        }
        println();
        System.out.println("Поиск отрицательных чисел в массиве");

        while (true) {
            System.out.print("Введите индекс с которого начнём поиск отрицательных чисел: ");
            int number = scanner.nextInt();
            if (number < 0 || number > array.length) {
                System.out.println("Ввели несуществующий интекс. Повторите попытку.");
            } else {
                fromOddNumber = number;
                break;
            }
        }

        while (true) {
            System.out.print("Введите индекс до которого будем искать отрицательные числа: ");
            int number = scanner.nextInt();
            if (number < 0 || number > array.length) {
                System.out.println("Ввели несуществующий интекс. Повторите попытку.");
            } else if (fromOddNumber > number) {
                System.out.println("Вы ввели индекс меньше первого. Повторите попытку.");
            } else {
                toOddNumber = number;
                break;
            }
        }

        println();
        EvenNumbersPrintTask evenNumbersPrintTask = new EvenNumbersPrintTask(fromEvenNumber, toEvenNumber, array);
        OddNumbersPrintTask oddNumbersPrintTask = new OddNumbersPrintTask(fromOddNumber, toOddNumber, array);

        Task[] tasks = {evenNumbersPrintTask, oddNumbersPrintTask};
        completeAllTasks(tasks);
    }

    public static void completeAllTasks(Task[] tasks) {
        for (int i = 0; i < tasks.length; i++) {
            tasks[i].complete();
        }
    }

    public static void println() {
        System.out.println();
    }

}