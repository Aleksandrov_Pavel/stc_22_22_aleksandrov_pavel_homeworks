public class EvenNumbersPrintTask extends AbstractNumbersPrintTask {
    private int from;
    private int to;
    int[] array;
    private int flag = 0;

    public EvenNumbersPrintTask(int from, int to, int[] array) {
        super(from, to);
        this.from = from;
        this.to = to;
        this.array = array;
    }

    // Создаём метод для выведения чётных чисел
    public void complete() {
        System.out.print("Чётные числа: ");
        for (int i = from; i < to; i++) {
            if (array[i] % 2 == 0) {
                System.out.print(array[i] + " ");
                flag++;
            }
        }
        if (flag == 0) {
            System.out.println("Их нет");
        }
        println();
    }

    public static void println() {
        System.out.println();
    }

}