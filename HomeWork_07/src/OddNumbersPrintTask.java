public class OddNumbersPrintTask extends AbstractNumbersPrintTask {
    private int from;
    private int to;
    int[] array;
    private int flag = 0;

    public OddNumbersPrintTask(int from, int to, int[] array) {
        super(from, to);
        this.from = from;
        this.to = to;
        this.array = array;
    }

    // Создаём метод для выведния нечётных чисел
    public void complete() {
        System.out.print("Нечётные числа: ");
        for (int i = from; i < to; i++) {
            if (array[i] % 2 != 0) {
                System.out.print(array[i] + " ");
                flag++;
            }
        }
        if (flag == 0) {
            System.out.println("Их нет");
        }
        println();
    }

    public static void println() {
        System.out.println();
    }
}
