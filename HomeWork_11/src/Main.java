public class Main {
    public static void main(String[] args) {

        CarsRepository carsRepository = new CarsRepositoryFileBasedImpl("cars.txt");

        System.out.println(carsRepository.carIsBlackOrZeroMileage());
        System.out.println(carsRepository.numberOfUniqueModelsInThePriceRange());
        System.out.println(carsRepository.colorOfTheCarWithTheMinimumCost());
        System.out.println(carsRepository.averageCostOfCar());
    }
}