import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.Comparator;
import java.util.List;
import java.util.Objects;
import java.util.function.Function;
import java.util.stream.Collectors;


public class CarsRepositoryFileBasedImpl implements CarsRepository {

    private final String fileName;

    public CarsRepositoryFileBasedImpl(String fileName) {
        this.fileName = fileName;
    }

    private static final Function<String, Car> stringToCarMapper = currentCar -> {
        String[] parts = currentCar.split("\\|");
        String carNumber = parts[0];
        String carModel = parts[1];
        String carColor = parts[2];
        Integer carMileage = Integer.parseInt(parts[3]);
        Integer carPrice = Integer.parseInt(parts[4]);
        return new Car(carNumber, carModel, carColor, carMileage, carPrice);
    };


    @Override
    public List<String> carIsBlackOrZeroMileage() {
        try (BufferedReader reader = new BufferedReader(new FileReader(fileName))) {
            return reader.lines()
                    .map(stringToCarMapper)
                    .filter(car -> Objects.equals(car.getCarColor(), "Black") || car.getCarMileage() == 0)
                    .map(Car::getCarNumber)
                    .collect(Collectors.toList());
        } catch (IOException e) {
            throw new UnsuccessfulWorkWithFileException(e);
        }
    }

    @Override
    public int numberOfUniqueModelsInThePriceRange() {
        try (BufferedReader reader = new BufferedReader(new FileReader(fileName))) {
            return (int) reader.lines()
                    .map(stringToCarMapper)
                    .filter(car -> car.getCarPrice() >= 700000 && car.getCarPrice() <= 800000)
                    .map(Car::getCarModel)
                    .distinct()
                    .count();
        } catch (IOException e) {
            throw new UnsuccessfulWorkWithFileException(e);
        }
    }

    @Override
    public String colorOfTheCarWithTheMinimumCost() {
        try (BufferedReader reader = new BufferedReader(new FileReader(fileName))) {
            return reader.lines()
                    .map(stringToCarMapper)
                    .min(Comparator.comparingInt(Car::getCarPrice))
                    .map(Car::getCarColor)
                    .get();

        } catch (IOException e) {
            throw new UnsuccessfulWorkWithFileException(e);
        }

    }

    @Override
    public double averageCostOfCar() {
        try (BufferedReader reader = new BufferedReader(new FileReader(fileName))) {
            return reader.lines()
                    .map(stringToCarMapper)
                    .filter(car -> Objects.equals(car.getCarModel(), "Camry"))
                    .mapToInt(Car::getCarPrice)
                    .average()
                    .getAsDouble();

        } catch (IOException e) {
            throw new UnsuccessfulWorkWithFileException(e);
        }

    }
}
