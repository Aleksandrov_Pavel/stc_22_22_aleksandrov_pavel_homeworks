import java.util.List;

public interface CarsRepository {

    List<String> carIsBlackOrZeroMileage();

    public int numberOfUniqueModelsInThePriceRange();

    public String colorOfTheCarWithTheMinimumCost();

    public double averageCostOfCar();

}
