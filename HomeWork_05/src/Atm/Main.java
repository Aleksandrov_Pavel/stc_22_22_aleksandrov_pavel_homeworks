package Atm;

public class Main {
    public static void main(String[] args) {
        // Создаём объект класса AtmMachine
        AtmMachine atm = new AtmMachine(5000, 10000, 20000);
        // Запрашиваем сумму для вывода из банкомата
        System.out.println(atm.issuanceOfMoney(11000));
        // Вносим сумму
        System.out.println(atm.depositingMoney(40000));
        // Проверяем количество проведённых операций
        System.out.println(atm.getNumberOfOperations());
        // Проверяем баланс
        System.out.println(atm.getBalanceAtm());
    }
}