package Atm;

public class AtmMachine {

    // Создаём переменную "Сумма денег в банкомате"
    private int balanceAtm;
    // Создаём переменную "Максимальная сумма к выдачи"
    private final int maxAmountToBeIssued;
    // Создаём переменную "Максимальная сумма для хранящаяся в банкомате"
    private final int maxAmountAtm;
    // Создаём переменную "Количество проведённых операций"
    private int numberOfOperations;

    // Создаём геттер просмотра суммы денег в банкомате
    public int getBalanceAtm() {
        return balanceAtm;
    }
    // Создаёс геттер для просмотра коллличества проведённых операций
    public int getNumberOfOperations() {
        return numberOfOperations;
    }

    // Создаём конструктор класса AtmMachine
    AtmMachine(int balanceAtm, int maxAmountToBeIssued, int maxAmountAtm) {
        this.balanceAtm = balanceAtm;
        this.maxAmountToBeIssued = maxAmountToBeIssued;
        this.maxAmountAtm = maxAmountAtm;
    }

    // Метод выдачи денег из банкомата
    public int issuanceOfMoney(int amountOfMoney) {
        // Увеличиваем значение в переменной "Количество проведённых операций", на одну операцию
        numberOfOperations++;
        // Если запрашиваемая сумма больше максимальной суммы к выдачи
        if (amountOfMoney > maxAmountToBeIssued) {
            // Если запрашиваемая сумма больше количества денег в банкомате
            if(amountOfMoney > balanceAtm) {
                amountOfMoney = balanceAtm;
                balanceAtm = 0;
                return amountOfMoney;
            }
            // Если не удовлетворяет условию, то выводим максимальную сумму к выдаче
            balanceAtm -= maxAmountToBeIssued;
            return maxAmountToBeIssued;
        // Иначе, если запрашиваемая сумма больше количества денег в банкомате
        } else if (amountOfMoney > balanceAtm) {
            amountOfMoney = balanceAtm;
            balanceAtm = 0;
            // Выводим максимальное количество денег в банкомате
            return amountOfMoney;
        // Иначе
        } else {
            balanceAtm -= amountOfMoney;
            return amountOfMoney;
        }
    }

    // Метод внесения денег на счёт
    public int depositingMoney(int amountOfMoney) {
        numberOfOperations++;
        if ((amountOfMoney + balanceAtm) > maxAmountAtm) {
            // Создаём переменную хранящую не положенную сумму в банкомат
            int newMoney = amountOfMoney + balanceAtm - maxAmountAtm;
            balanceAtm = maxAmountAtm;
            return newMoney;
        }
        balanceAtm += amountOfMoney;
        return 0;
    }
}