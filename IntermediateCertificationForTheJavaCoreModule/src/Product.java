import java.util.Objects;

public class Product {
    private Integer id;
    private String title;
    private Double cost;
    private Integer quantityInStock;

    public Product(Integer id, String title, Double cost, Integer quantityInStock) {
        this.id = id;
        this.title = title;
        this.cost = cost;
        this.quantityInStock = quantityInStock;
    }

    public Integer getId() {
        return id;
    }

    public String getTitle() {
        return title;
    }

    public Double getCost() {
        return cost;
    }

    public Integer getQuantityInStock() {
        return quantityInStock;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public void setCost(Double cost) {
        this.cost = cost;
    }

    public void setQuantityInStock(Integer quantityInStock) {
        this.quantityInStock = quantityInStock;
    }

    @Override
    public String toString() {
        return "Product {" +
                " id = " + id +
                ", title = '" + title + '\'' +
                ", cost = " + cost +
                ", quantityInStock = " + quantityInStock +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Product product = (Product) o;
        return id.equals(product.id) && title.equals(product.title) && cost.equals(product.cost) && quantityInStock.equals(product.quantityInStock);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, title, cost, quantityInStock);
    }
}
