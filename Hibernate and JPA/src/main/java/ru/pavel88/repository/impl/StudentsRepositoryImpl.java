package ru.pavel88.repository.impl;

import jakarta.persistence.EntityManager;
import jakarta.persistence.EntityTransaction;
import lombok.RequiredArgsConstructor;
import ru.pavel88.models.Course;
import ru.pavel88.models.Student;
import ru.pavel88.repository.StudentsRepository;

import java.util.List;

// аннатация @RequiredArgsConstructor создаёт конструктор класса на имеющиеся поля

@RequiredArgsConstructor
public class StudentsRepositoryImpl implements StudentsRepository {

    // создаём объектную переменную
    private final EntityManager entityManager;

    @Override
    public void save(Student entity) {
        // получаю текущую сеесию
        EntityTransaction transaction = entityManager.getTransaction();

        // открываем транзыкцию
        transaction.begin();

        // кладём в объектную переменную объект
        entityManager.persist(entity);

        // закрываем транзакцию и записываем результат в БД
        transaction.commit();

    }

    @Override
    public List<Student> findAll() {
        return null;
    }

    @Override
    public Student findById(Long id) {
        return entityManager.find(Student.class, id);
    }
}
