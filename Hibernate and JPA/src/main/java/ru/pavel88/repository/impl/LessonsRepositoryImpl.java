package ru.pavel88.repository.impl;

import jakarta.persistence.EntityManager;
import jakarta.persistence.EntityTransaction;
import lombok.RequiredArgsConstructor;
import ru.pavel88.models.Course;
import ru.pavel88.models.Lesson;
import ru.pavel88.repository.LessonsRepository;

import java.util.List;

// аннатация @RequiredArgsConstructor создаёт конструктор класса на имеющиеся поля

@RequiredArgsConstructor
public class LessonsRepositoryImpl implements LessonsRepository {

    // создаём объектную переменную
    private final EntityManager entityManager;

    @Override
    public void save(Lesson entity) {
        // Получаем текущую транзакцию
        EntityTransaction transaction = entityManager.getTransaction();

        // открываем транзакцию
        transaction.begin();

        // сохраняем объект entity в объектную переменную
        entityManager.persist(entity);

        // закрываем транзакцию и записываем результат в БД
        transaction.commit();
    }

    @Override
    public List<Lesson> findAll() {
        return null;
    }

    @Override
    public Lesson findById(Long id) {
        return entityManager.find(Lesson.class, id);
    }
}
