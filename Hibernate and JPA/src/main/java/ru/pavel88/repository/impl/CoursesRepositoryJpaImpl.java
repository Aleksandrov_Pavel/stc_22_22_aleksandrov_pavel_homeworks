package ru.pavel88.repository.impl;

import jakarta.persistence.EntityManager;
import jakarta.persistence.EntityTransaction;
import lombok.AllArgsConstructor;
import lombok.RequiredArgsConstructor;
import ru.pavel88.models.Course;
import ru.pavel88.repository.CoursesRepository;

import java.util.List;

// аннатация @RequiredArgsConstructor создаёт конструктор класса на имеющиеся поля

@RequiredArgsConstructor
public class CoursesRepositoryJpaImpl implements CoursesRepository {

    // Создаём объектную переменную
    private final EntityManager entityManager;

    @Override
    public void save(Course entity) {
        // Получаем текущую транзакцию
        EntityTransaction transaction = entityManager.getTransaction();

        // открываем тарнзакцию
        transaction.begin();

        // сохранияем вносимый объект в объектную переменную
        entityManager.persist(entity);

        // закрываем транзакцию и записываем результат в БД
        transaction.commit();

    }

    @Override
    public List<Course> findAll() {
        return entityManager.createQuery("select course from Course course", Course.class).getResultList();
    }

    @Override
    public Course findById(Long id) {
        return entityManager.find(Course.class, id);
    }
}
