package ru.pavel88.repository;

import ru.pavel88.models.Lesson;

public interface LessonsRepository extends CrudRepository<Lesson> {
}
