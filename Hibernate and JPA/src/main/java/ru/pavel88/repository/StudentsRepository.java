package ru.pavel88.repository;

import ru.pavel88.models.Student;

public interface StudentsRepository extends CrudRepository<Student> {

}
