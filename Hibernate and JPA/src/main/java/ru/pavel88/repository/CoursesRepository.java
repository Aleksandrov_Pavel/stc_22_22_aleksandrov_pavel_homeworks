package ru.pavel88.repository;

import ru.pavel88.models.Course;

public interface CoursesRepository extends CrudRepository<Course>{
}
