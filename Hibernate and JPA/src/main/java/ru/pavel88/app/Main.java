package ru.pavel88.app;

import jakarta.persistence.EntityManager;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;
import ru.pavel88.models.Course;
import ru.pavel88.models.Lesson;
import ru.pavel88.models.Student;
import ru.pavel88.repository.*;
import ru.pavel88.repository.impl.CoursesRepositoryJpaImpl;
import ru.pavel88.repository.impl.LessonsRepositoryImpl;
import ru.pavel88.repository.impl.StudentsRepositoryImpl;

import java.time.LocalDate;
import java.time.LocalTime;

public class Main {
    public static void main(String[] args) {
        // создаём объект класса Configuration (из билиотеки Hibernate)
        Configuration configuration = new Configuration();
        // кладём в объект файл с заданными пораметрами
        configuration.configure("hibernate\\hibernate.cfg.xml");

        // создаём сессию
        SessionFactory sessionFactory = configuration.buildSessionFactory();

        // Создаём обектную переменную
        EntityManager entityManager = sessionFactory.createEntityManager();

        // создаём объект класса и кладём в него entityManager
        CoursesRepository coursesRepository = new CoursesRepositoryJpaImpl(entityManager);
        // создаём объект класса и кладём в него entityManager
        StudentsRepository studentsRepository = new StudentsRepositoryImpl(entityManager);
        // создаём обект класса и кладём в него entityManager
        LessonsRepository lessonsRepository = new LessonsRepositoryImpl(entityManager);


//        // создаём объект
//        Course java = Course.builder()
//                .description("Курс по Java")
//                .title("Java")
//                // Текущее время
//                .start(LocalDate.now())
//                // Прибавить к текущей дате 6 месяцев
//                .finish(LocalDate.now().plusMonths(6))
//                .build();
//
//        Course php = Course.builder()
//                .description("Курс по PHP")
//                .title("PHP")
//                // Отнять от текущего время 1 месяц
//                .start(LocalDate.now().minusMonths(1))
//                // Прибавить к текущей дате 3 месяцев
//                .finish(LocalDate.now().plusMonths(3))
//                .build();
//
//        // вызываем у coursesRepository метод save и кладём объкты
//        coursesRepository.save(java);
//        coursesRepository.save(php);
//
//
//        // у объета вызываем метод findAll() и выводим результат
//        System.out.println(coursesRepository.findAll());


/**
 // находим в coursesRepository данные по второму id
 System.out.println(coursesRepository.findById(2L));
 **/

        Course course = coursesRepository.findById(2L);
 //       Student student = studentsRepository.findById(1L);
//
//        // создаём урок
//        Lesson lesson = Lesson.builder()
//                // привязываем курс (course) к которому привязан создаваемый урок
//                .course(course)
//                .startTime(LocalTime.of(16, 0))
//                // когда заканчивается урок
//                .finishTime(LocalTime.of(17, 0))
//                .summary("Создание сайтов")
//                .name("PHP - 1")
//                .build();
//
//        // вызываем у lessonsRepository метод save и кладём объкты
//        lessonsRepository.save(lesson);
//
//        // добавляем в сущность student курс
//        student.getCourses().add(course);
//        // вносим новые данные в поле Age
//        student.setAge(33);
//
//        // перезаписываем изменения
//        studentsRepository.save(student);
//

         //закрываем ссессию
        entityManager.close();

        System.out.println(course);


    }
}