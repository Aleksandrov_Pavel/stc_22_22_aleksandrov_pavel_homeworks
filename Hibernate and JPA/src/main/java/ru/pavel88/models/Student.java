package ru.pavel88.models;

import jakarta.persistence.*;
import lombok.*;
import org.hibernate.annotations.Check;

import java.util.Set;

// аннатация @Entity это JPA (работа с БД)
// аннотации перед id: @Id и @GeneratedValue(strategy = GenerationType.IDENTITY) говорят "id" генерироватся БД-ой
// аннатация @Column(name = ) как будет называться в БД, в таблицe колонка
// аннатация @Column(unique = true, nullable = false). unique уникальность записи, nullable = true - NOT NULL
// аннатация @Check(constraints = "age >= 0 and age <= 120"), ограничение заполнения в столюцу (age название столбца)
// аннотация @ManyToMany свзь многие ко многим
/** Создание таблицы с указание полей: @JoinTable(joinColumns = {@JoinColumn(name = "student_id", referencedColumnName = "id")},
*    inverseJoinColumns = {@JoinColumn(name = "course_id", referencedColumnName = "id")})
 *
 *    @JoinTable - создание таблицы
 *    @JoinColumn - создание колонки
 *    name - название колонки
 *    referencedColumnName - имя столбца в таблице, на который делается ссылка со столбцом, который аннонсируется
 **/
// аннатация  @EqualsAndHashCode(exclude = {"courses"}) создаём исключение на обработку пол courses
// аннатация @ToString(exclude = {"courses"}) создаём исключение на обработку поля courses

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@EqualsAndHashCode(exclude = {"courses"})
@ToString(exclude = {"courses"})
@Builder
@Entity
public class Student {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(unique = true, nullable = false)
    private String email;

    private String password;

    @Column(name = "first_name")
    private String firstName;

    @Column(name = "last_name")
    private String lastName;

    @Check(constraints = "age >= 0 and age <= 120")
    private Integer age;

    @Column(name = "is_worker")
    private Boolean isWorker;

    private Double average;

    // создаём связь в объектно-ориентированном стиле
    // создаём список(множество) курсов
    @ManyToMany
    @JoinTable(joinColumns = {@JoinColumn(name = "student_id", referencedColumnName = "id")},
    inverseJoinColumns = {@JoinColumn(name = "course_id", referencedColumnName = "id")})
    private Set<Course> courses;

}
