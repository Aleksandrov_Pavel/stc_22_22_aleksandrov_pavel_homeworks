package ru.pavel88.models;

import jakarta.persistence.*;
import lombok.*;

import java.time.LocalTime;

// аннатация @Entity это JPA (работа с БД)
// аннотации перед id: @Id и @GeneratedValue(strategy = GenerationType.IDENTITY) говорят "id" генерироватся БД-ой
// аннатация @Column(name = ) как будет называться в БД, в таблицe колонка
// аннотация @ManyToOne и @JoinColumn(name = "course_id") связь много уроков у одного курса
// аннатация  @EqualsAndHashCode(exclude = {"courses"}) создаём исключение на обработку пол courses
// аннатация @ToString(exclude = {"courses"}) создаём исключение на обработку поля courses

@AllArgsConstructor
@NoArgsConstructor
@Setter
@Getter
@EqualsAndHashCode(exclude = "course")
@ToString(exclude = "course")
@Builder
@Entity
public class Lesson {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String name;
    private String summary;

    @Column(name = "start_time")
    private LocalTime startTime;

    @Column(name = "finish_time")
    private LocalTime finishTime;

    // создаём связь в объектно-ориентированном стиле
    // создаём обектную переменную для связи с Course
    @ManyToOne
    @JoinColumn(name = "course_id")
    private Course course;
}
