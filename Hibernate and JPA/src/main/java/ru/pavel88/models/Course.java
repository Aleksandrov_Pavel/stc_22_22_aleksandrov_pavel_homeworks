package ru.pavel88.models;

import jakarta.persistence.*;
import lombok.*;

import java.time.LocalDate;
import java.util.Set;

// аннатация @Entity это JPA (работа с БД)
// аннотации перед id: @Id и @GeneratedValue(strategy = GenerationType.IDENTITY) говорят "id" генерироватся БД-ой
// аннотация @OneToMany(mappedBy = "course", fetch = FetchType.EAGER) связь 1 курс много уроков и просим с помощью
//      fetch = FetchType.EAGER вытащить всё связанное с курсом
// аннотация @ManyToMany(mappedBy = "courses", fetch = FetchType.EAGER) свзь многие ко многим и просим с помощью
//      fetch = FetchType.EAGER вытащить всё связанное с курсом
// аннатация  @EqualsAndHashCode(exclude = {"courses"}) создаём исключение на обработку пол courses
// аннатация @ToString(exclude = {"courses"}) создаём исключение на обработку поля courses

@AllArgsConstructor
@NoArgsConstructor
@Setter
@Getter
@EqualsAndHashCode(exclude = {"lessons", "students"})
@ToString
@Builder
@Entity
public class Course {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String title;

    @Column(length = 1000)
    private String description;

    private LocalDate start;
    private LocalDate finish;

    // создаём связь в объектно-ориентированном стиле
    // создаём (связь) список(множество) уроков
    @OneToMany(mappedBy = "course", fetch = FetchType.EAGER)
    private Set<Lesson> lessons;

    // создаём связь в объектно-ориентированном стиле
    // создаём список(множество) студентов записанных на курсы
    @ManyToMany(mappedBy = "courses", fetch = FetchType.EAGER)
    private Set<Student> students;
}

