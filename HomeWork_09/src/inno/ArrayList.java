package inno;

public class ArrayList<T> implements List<T> {
    // Создаём константу
    private final static int DEFAULT_ARRAY_SIZE = 10;

    // поле-массив, которое хранит все элементы
    private T[] elements;

    // поле, которое хранит число элементов
    private int count;

    // Создаём конструктор класса
    public ArrayList() {
        this.elements = (T[]) new Object[DEFAULT_ARRAY_SIZE];
        this.count = 0;
    }

    @Override
    public void add(T element) {
        // Если массив заполнен
        if (count == elements.length) {
            // Берем старый размер массива
            int currentLength = elements.length;
            // Получаем длинну массива в полтора раза больше предыдущей длинны масиива
            int newLength = currentLength + currentLength / 2;
            // Создаём новый массив с новой длинной массива
            T[] newElements = (T[]) new Object[newLength];

            // Запоняем новый массив из старого массива
            for (int i = 0; i < count; i++) {
                newElements[i] = elements[i];
            }
            // Переключаем ссылку на новый массив
            this.elements = newElements;
        }
        if (count < elements.length) {
            elements[count] = element;
            count++;
        }

    }

    @Override
    public boolean contains(T element) {
        // Пробегаем по массиву
        for (int i = 0; i < count; i++) {
            // Если элемент найден
            if (elements[i].equals(element)) {
                return true;
            }
        }
        // Если элемент так и не найден - возвращаем false
        return false;
    }

    @Override
    public int size() {
        return count;
    }

    @Override
    public T get(int index) {
        // Если index больше или равер 0 и меньше count
        if (index >= 0 && index < count) {
            // То выводим элемент находящийся под index
            return elements[index];
        }
        // Если значение index не удовлетворяет условию, то выводим null
        return null;
    }

    @Override
    public void remove(T element) {
        // Проверяем наличие элемента
        for (int m = 0; m < count; m++) {
            // Если нашли элемент
            if (elements[m] == element) {
                // Получаем длинну нового массива
                int newLength = elements.length;
                // Создаём новый массив с новой длинной массива
                T[] newElements = (T[]) new Object[newLength];
                // Записываем новый массив
                for (int i = 0; i < count; i++) {
                    if (elements[i] == element) {
                        for (int j = 0; j < i; j++) {
                            newElements[j] = elements[j];
                        }
                        for (int o = i; o < count - 1; o++) {
                            newElements[o] = elements[o + 1];
                        }
                        break;
                    }
                }
                // Переназначаем массив
                this.elements = newElements;
                // Перезаписываем поле, которое хранит число элементов
                this.count = count - 1;
                break;
            }
        }
    }

    @Override
    public void removeAt(int index) {
        // Проверяем наличие индекса
        if (index >= 0 && index < count) {
            // Получаем длинну нового массива
            int newLength = elements.length;
            // Создаём новый массив с новой длинной массива
            T[] newElements = (T[]) new Object[newLength];
            // Записываем новый массив
            for (int i = index; i < count; i++) {
                for (int j = 0; j < i; j++) {
                    newElements[j] = elements[j];
                }
                for (int o = i; o < count - 1; o++) {
                    newElements[o] = elements[o + 1];
                }
                break;
            }
            // Переназначаем массив
            this.elements = newElements;
            // Перезаписываем поле, которое хранит число элементов
            this.count = count - 1;
        }
    }
}
