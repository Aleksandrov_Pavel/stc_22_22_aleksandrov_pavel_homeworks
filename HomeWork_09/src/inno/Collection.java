package inno;

public interface Collection<T> {
    /**
     * Добавляем элемент в коллекцию
     *
     * @param element добавляемый элемент
     */
    void add(T element);

    /**
     * Удаляем элемент из коллекции
     *
     * @param element удаляемый элемент с помощью <code>boolean equals</code>
     */
    void remove(T element);

    /**
     * Проверяем наличие элемента в коллекции
     *
     * @param element проверяемый элемент
     * @return <code>true</code>, если элемент найден
     * <code>false</code>, если элемент не найден
     */
    boolean contains(T element);


    /**
     * Возвращает количество элементов в коллекции
     *
     * @return число, равное количеству добавленных элементов
     */
    int size();
}
