package inno;

public class Main {
    public static void main(String[] args) {


        // Создаём объект ArrayList
        List<Integer> integerArrayList = new ArrayList<>();

        // Заполняем массив
        integerArrayList.add(8);
        integerArrayList.add(10);
        integerArrayList.add(11);
        integerArrayList.add(13);
        integerArrayList.add(11);
        integerArrayList.add(15);
        integerArrayList.add(-5);

        // Вызываем метод remove
        integerArrayList.remove(11);
        // Выводим результат
        for (int i = 0; i < integerArrayList.size(); i++) {
            System.out.print(integerArrayList.get(i) + " ");
        }
        // Делаем переход на новую строку
        System.out.println();

        // Вызываем метод removeAt
        integerArrayList.removeAt(5);
        // Выводим результат
        for (int i = 0; i < integerArrayList.size(); i++) {
            System.out.print(integerArrayList.get(i) + " ");
        }
        //  Делаем переход на новую строку
        System.out.println();

        // Создаём объект LinkedList
        List<Integer> integerLinkedList = new LinkedList<>();

        // Заполняем массив
        integerLinkedList.add(12);
        integerLinkedList.add(55);
        integerLinkedList.add(8);
        integerLinkedList.add(13);
        integerLinkedList.add(8);
        integerLinkedList.add(-3);
        integerLinkedList.add(19);

        // Вызываем метод remove
        integerLinkedList.remove(8);
        // Выводим результат
        for (int i = 0; i < integerLinkedList.size(); i++) {
            System.out.print(integerLinkedList.get(i) + " ");
        }
        // Делаем переход на новую строку
        System.out.println();

        // Вызываем метод removeAt
        integerLinkedList.removeAt(5);
        // Выводим результат
        for (int i = 0; i < integerLinkedList.size(); i++) {
            System.out.print(integerLinkedList.get(i) + " ");
        }
    }

}