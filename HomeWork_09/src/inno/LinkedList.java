package inno;

public class LinkedList<T> implements List<T> {

    // Создаём ссылку на первый элемент
    private Node<T> first;
    // Создаём ссылку на последний элемент
    private Node<T> last;
    // Создаём
    private int count = 0;

    // Создаём вложенный private класс
    private static class Node<E> {
        // Поздаём переменную для хранения значения
        E value;
        // Создаём обектную переменную (узел) хранящую ссылку на другой обект
        Node<E> next;

        // Создаём конструктор класса
        public Node(E value) {
            this.value = value;
        }
    }

 /*   РЕАЛИЗАЦИЯ МЕТОДА "add" ПЕРВЫЙ ВАРИАНТ
    @Override
    public void add(T element) {
        // Создаём новый узел и кладём в него значение
        Node<T> newNode = new Node<>(element);
        // Если элементов в списке нет (count = 0)
        if (count == 0) {
            // Кладём новый узел в качестве первого
            this.first = newNode;
        } else {
        // Иначе мы должны дойти по последнего узла
            // Запоминаем ссылку на первый узел
            Node<T> current = this.first;
            // Пока недошли до узла, у которого нет следующего узла (до последнего)
            while (current.next != null) {
                // Запоминаем ссылку
                current = current.next;
            }
            // Теперь у нас current указывает на последний узел

            // Дедаем следующий после последнего - новый узел
            current.next = newNode;
        }
        count++;
    }
*/

    @Override
    public void add(T element) {
        // Создаём новый узел и кладём в него значение
        Node<T> newNode = new Node<>(element);
        // Если элементов в списке нет (count = 0)
        if (count == 0) {
            // Кладём новый узел в качестве первого и последнего
            this.first = newNode;
        } else {
            // Если в списке уже есть узлы
            // У последнего делаем следующим новый узел
            this.last.next = newNode;
        }
        this.last = newNode;
        count++;
    }

    @Override
    public boolean contains(T element) {
        // Получить ссылку на первый элемент списка
        // пройтись по всем элементам списка
        Node<T> current = this.first;
        // Пока не обошли весь список
        while (current != null) {
            // Если значение в текущем узле совпало с искомым
            if (current.value.equals(element)) {
                return true;
            } else {
                // Если не совпало - идём к следующему узлу
                current = current.next;
            }
        }
        // если не нашли элемент выводим false
        return false;
    }

    @Override
    public int size() {
        return count;
    }

    @Override
    public T get(int index) {
        if (0 <= index && index <= count) {
            // Начинаем с первого элемента
            Node<T> current = this.first;

            // Если запрошенный индекс был равен - 3
            // То, будем переберать 0, 1, 2
            for (int i = 0; i < index; i++) {
                // На каждом шаге цикла двигаемся дальше
                current = current.next;
            }
            return current.value;
        }
        return null;
    }

    @Override
    public void remove(T element) {
        // Получить ссылку на первый элемент списка
        Node<T> current = this.first;
        // Переменная для получение ссылки на предыдущий элемент
        Node<T> previous = null;
        // Создаём счётчик операций
        int flag = 0;

        // пройтись по всем элементам списка
        while (current != null) {
            // Если значение в текущем узле совпало с искомым
            if (current.value.equals(element)) {
                // Если значение находится первым по счёту элементе
                if (flag == 0) {
                    // в переменную кладём ссылку на второй элемент
                    this.first = current.next;
                    // у первого элемента в обектрую переменную кладём null
                    current.next = null;
                    // уменьшаем количество элементов
                    this.count -= 1;
                    break;
                    // Если значение находится в последнем элементе
                } else if (flag == count) {
                    // В предпоследнем элементе, в объектрную переменную кладём null
                    previous.next = null;
                    // Уменьшаем количество элементов
                    this.count -= 1;
                    break;
                    // Если не совпало
                } else {
                    // В предпоследнем элементе, в объектрную переменную кладём
                    // значение объектной переменной текущего элемента
                    previous.next = current.next;
                    // В текущем элементе, в объектрую переменную кладём null
                    current.next = null;
                    // Уменьшаем количество элементов
                    this.count -= 1;
                    break;
                }
                // Если не совпало
            } else {
                // В предпоследнюю объектную переменню кладём значение текущей
                previous = current;
                // Идём к следующему узлу
                current = current.next;
            }
            flag++;
        }
    }


    @Override
    public void removeAt(int index) {
        if (0 <= index && index < count) {
            // Начинаем с первого элемента
            Node<T> current = this.first;
            // Переменная для получение ссылки на предыдущий элемент
            Node<T> previous = null;

            // Ищим что находится под index
            for (int i = 0; i <= index; i++) {
                // Если значение в текущем узле совпало с искомым
                if (i == index) {
                    // Если значение находится первым по счёту элементе
                    if (index == 0) {
                        // в переменную кладём ссылку на второй элемент
                        this.first = current.next;
                        // у первого элемента в обектрую переменную кладём null
                        current.next = null;
                        // уменьшаем количество элементов
                        this.count -= 1;
                        break;
                        // Если значение находится в последнем элементе
                    } else if (index == count - 1) {
                        // В предпоследнем элементе, в объектрную переменную кладём null
                        previous.next = null;
                        // Уменьшаем количество элементов
                        this.count -= 1;
                        break;
                        // Если не совпало
                    } else {
                        // В предпоследнем элементе, в объектрную переменную кладём
                        // значение объектной переменной текущего элемента
                        previous.next = current.next;
                        // В текущем элементе, в объектрую переменную кладём null
                        current.next = null;
                        // Уменьшаем количество элементов
                        this.count -= 1;
                        break;
                    }
                    // Если не совпало
                } else {
                    // В предпоследнюю объектную переменню кладём значение текущей
                    previous = current;
                    // Идём к следующему узлу
                    current = current.next;
                }
            }
        }
    }
}
