package ru.pva88.webapp.services;

import ru.pva88.webapp.dto.CoursesForm;
import ru.pva88.webapp.models.Course;
import ru.pva88.webapp.models.Lesson;

import java.util.List;

public interface CoursesService {

    List<Course> getAllCourses();

    void addCourses(CoursesForm courses);

    Course getCourse(Long courseId);

    void updateCourses(Long coursesId, CoursesForm course);

    void deleteCourses(Long coursesId);

    void addLessonToCourse(Long courseId, Long lessonId);

    List<Lesson> getNotInCourseLesson();

    List<Lesson> getInCourseLesson(Long courseId);


}