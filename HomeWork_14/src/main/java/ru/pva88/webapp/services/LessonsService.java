package ru.pva88.webapp.services;

import ru.pva88.webapp.dto.LessonsForm;
import ru.pva88.webapp.models.Lesson;

import java.util.List;

public interface LessonsService {

    List<Lesson> getAllLessons();

    void addLessons(LessonsForm lessons);

    Lesson getLesson(Long lessonId);

    void updateLessons(Long lessonsId, LessonsForm lesson);

    void deleteLessons(Long lessonsId);

}


