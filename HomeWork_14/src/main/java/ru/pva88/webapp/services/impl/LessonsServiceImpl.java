package ru.pva88.webapp.services.impl;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import ru.pva88.webapp.dto.LessonsForm;
import ru.pva88.webapp.models.Lesson;
import ru.pva88.webapp.repositories.LessonsRepository;
import ru.pva88.webapp.services.LessonsService;

import java.time.LocalTime;
import java.util.List;


@Service
@RequiredArgsConstructor
public class LessonsServiceImpl implements LessonsService {

    private final LessonsRepository lessonsRepository;

    @Override
    public List<Lesson> getAllLessons() {
        return lessonsRepository.findAllByStateNot(Lesson.State.DELETED);
    }

    @Override
    public void addLessons(LessonsForm lesson) {
        Lesson newLesson = Lesson.builder()
                .name(lesson.getName())
                .summary(lesson.getSummary())
                .state(Lesson.State.CONFIRMED)
                .startTime(LocalTime.of(00, 00))
                .finishTime(LocalTime.of(00, 00))
                .build();
        lessonsRepository.save(newLesson);
    }

    @Override
    public Lesson getLesson(Long lessonId) {
        return lessonsRepository.findById(lessonId).orElseThrow();
    }

    @Override
    public void updateLessons(Long lessonId, LessonsForm updateLesson) {
        Lesson lessonForUpdate = lessonsRepository.findById(lessonId).orElseThrow();
        lessonForUpdate.setName(updateLesson.getName());
        lessonForUpdate.setSummary(updateLesson.getSummary());
        lessonForUpdate.setStartTime(updateLesson.getStartTime());
        lessonForUpdate.setFinishTime(updateLesson.getFinishTime());
        lessonsRepository.save(lessonForUpdate);
    }

    @Override
    public void deleteLessons(Long lessonIs) {
        Lesson lessonsForDeleted = lessonsRepository.findById(lessonIs).orElseThrow();
        lessonsForDeleted.setState(Lesson.State.DELETED);
        lessonsForDeleted.setCourse(null);
        lessonsRepository.save(lessonsForDeleted);
    }

}
