package ru.pva88.webapp.services.impl;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import ru.pva88.webapp.dto.CoursesForm;
import ru.pva88.webapp.models.Course;
import ru.pva88.webapp.models.Lesson;
import ru.pva88.webapp.repositories.CoursesRepository;
import ru.pva88.webapp.repositories.LessonsRepository;
import ru.pva88.webapp.services.CoursesService;

import java.time.LocalDate;
import java.util.List;

@Service
@RequiredArgsConstructor
public class CoursesServiceImpl implements CoursesService {

    private final CoursesRepository coursesRepository;
    private final LessonsRepository lessonsRepository;

    @Override
    public List<Course> getAllCourses() {
        return coursesRepository.findAllByStateNot(Course.State.DELETED);
    }

    @Override
    public Course getCourse(Long courseId) {
        return coursesRepository.findById(courseId).orElseThrow();
    }


    @Override
    public void deleteCourses(Long courseId) {
        Course courseForDelete = coursesRepository.findById(courseId).orElseThrow();
        courseForDelete.setState(Course.State.DELETED);
        coursesRepository.save(courseForDelete);
    }

    @Override
    public void addCourses(CoursesForm course) {
        Course newCourse = Course.builder()
                .title(course.getTitle())
                .description(course.getDescription())
                .start(LocalDate.EPOCH)
                .finish(LocalDate.EPOCH)
                .state(Course.State.CONFIRMED)
                .build();
        coursesRepository.save(newCourse);
    }

    @Override
    public void updateCourses(Long courseId, CoursesForm courses) {
        Course courseForUpdate = coursesRepository.findById(courseId).orElseThrow();
        courseForUpdate.setTitle(courses.getTitle());
        courseForUpdate.setDescription(courses.getDescription());
        courseForUpdate.setStart(courses.getStart());
        courseForUpdate.setFinish(courses.getFinish());
        coursesRepository.save(courseForUpdate);
    }


    @Override
    public List<Lesson> getNotInCourseLesson() {
        return lessonsRepository.findAllByCourseNullAndState(Lesson.State.CONFIRMED);
    }

    @Override
    public List<Lesson> getInCourseLesson(Long courseId) {
        Course course = coursesRepository.findById(courseId).orElseThrow();
        return lessonsRepository.findAllByCourse(course);
    }


    @Override
    public void addLessonToCourse(Long courseId, Long lessonId) {
        Course course = coursesRepository.findById(courseId).orElseThrow();
        Lesson lesson = lessonsRepository.findById(lessonId).orElseThrow();
        lesson.setCourse(course);
        lessonsRepository.save(lesson);
    }

}