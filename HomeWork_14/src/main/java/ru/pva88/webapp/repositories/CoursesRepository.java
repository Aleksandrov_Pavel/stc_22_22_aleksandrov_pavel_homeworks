package ru.pva88.webapp.repositories;


import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import ru.pva88.webapp.models.Course;

import java.util.List;

@Repository
public interface CoursesRepository extends JpaRepository<Course, Long> {

    List<Course> findAllByStateNot(Course.State state);


}

