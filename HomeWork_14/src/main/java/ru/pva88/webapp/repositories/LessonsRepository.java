package ru.pva88.webapp.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import ru.pva88.webapp.models.Course;
import ru.pva88.webapp.models.Lesson;

import java.util.List;

@Repository
public interface LessonsRepository extends JpaRepository<Lesson, Long> {

    List<Lesson> findAllByStateNot(Lesson.State state);

    List<Lesson> findAllByCourseNullAndState(Lesson.State state);

    List<Lesson> findAllByCourse(Course course);
}
