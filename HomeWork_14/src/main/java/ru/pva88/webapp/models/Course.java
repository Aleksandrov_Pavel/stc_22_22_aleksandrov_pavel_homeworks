package ru.pva88.webapp.models;

import lombok.*;

import javax.persistence.*;
import java.time.LocalDate;
import java.util.Set;

@Data
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode(exclude = {"lessons"})
@ToString
@Builder
@Entity
public class Course {

    public enum State {
        CONFIRMED, DELETED
    }

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String title;
    private String description;
    private LocalDate start;
    private LocalDate finish;

    @OneToMany(mappedBy = "course")
    private Set<Lesson> lessons;

    @Enumerated(value = EnumType.STRING)
    private State state;
}
