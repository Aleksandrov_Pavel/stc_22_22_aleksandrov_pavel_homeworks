package ru.pva88.webapp.controllers;


import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import ru.pva88.webapp.dto.CoursesForm;
import ru.pva88.webapp.services.CoursesService;


@Controller
@RequestMapping(value = "/courses")
@RequiredArgsConstructor
public class CoursesController {

    private final CoursesService coursesService;

    @GetMapping
    public String getCoursesPage(Model model) {
        model.addAttribute("courses", coursesService.getAllCourses());
        return "courses_page";
    }

    @PostMapping
    public String addCourses(CoursesForm courses) {
        coursesService.addCourses(courses);
        return "redirect:/courses";
    }

    @GetMapping("/{course-id}")
    public String getCoursePage(@PathVariable("course-id") Long courseId, Model model) {
        model.addAttribute("course", coursesService.getCourse(courseId));
        model.addAttribute("notInCourseLesson", coursesService.getNotInCourseLesson());
        model.addAttribute("inCourseLesson", coursesService.getInCourseLesson(courseId));
        return "course_page";
    }

    @PostMapping("/{course-id}/update")
    public String updateCourses(@PathVariable("course-id") Long coursesId, CoursesForm courses) {
        coursesService.updateCourses(coursesId, courses);
        return "redirect:/courses/" + coursesId;
    }

    @GetMapping("/{course-id}/delete")
    public String deleteCourses(@PathVariable("course-id") Long courseId) {
        coursesService.deleteCourses(courseId);
        return "redirect:/courses";
    }

    @PostMapping("/{course-id}/lessons")
    public String addLessonToCourse(@PathVariable("course-id") Long courseId,
                                    @RequestParam("lesson-id") Long lessonId) {
        coursesService.addLessonToCourse(courseId, lessonId);
        return "redirect:/courses/" + courseId;
    }

}
