package ru.pva88.webapp.controllers;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import ru.pva88.webapp.dto.LessonsForm;
import ru.pva88.webapp.services.LessonsService;

@RequiredArgsConstructor
@RequestMapping(value = "/lessons")
@Controller
public class LessonsController {

    private final LessonsService lessonsService;


    @GetMapping
    public String getLessonsPage(Model model) {
        model.addAttribute("lessons", lessonsService.getAllLessons());
        return "lessons_page";
    }

    @PostMapping
    public String addLessons(LessonsForm lessons) {
        lessonsService.addLessons(lessons);
        return "redirect:/lessons";
    }

    @GetMapping("/{lesson-id}")
    public String getLessonPage(@PathVariable("lesson-id") Long lessonId, Model model) {
        model.addAttribute("lesson", lessonsService.getLesson(lessonId));
        return "lesson_page";
    }

    @PostMapping("/{lesson-id}/update")
    public String updateLessons(@PathVariable("lesson-id") Long lessonsId, LessonsForm lessons) {
        lessonsService.updateLessons(lessonsId, lessons);
        return "redirect:/lessons/" + lessonsId;
    }

    @GetMapping("/{lesson-id}/delete")
    public String deleteLessons(@PathVariable("lesson-id") Long lessonsId) {
        lessonsService.deleteLessons(lessonsId);
        return "redirect:/lessons";
    }
}

