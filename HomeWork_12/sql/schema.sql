create schema Rent_Car;

create table Rent_Car.Users
(
    id_users                   bigserial primary key,
    Name                       varchar     not null,
    Surname                    varchar     not null,
    Phone_number               varchar(20) not null,
    Experience                 int         not null,
    Age                        int         not null,
    There_is_a_drivers_license bool        not null,
    Category_of_rights         varchar     not null,
    Rating                     int check (Rating >= 0 and Rating <= 5)
);

create table Rent_Car.Cars
(
    id_cars    bigserial primary key,
    Model      varchar not null,
    Colour     varchar not null,
    Car_number varchar not null,
    Owner_ID   int     not null
);

create table Rent_Car.Trip
(
    id_trip              bigserial primary key,
    Driver_ID            int  not null,
    Machine_ID           int  not null,
    Date_of_trip         date not null,
    Duration_of_the_trip time not null
);

alter table Rent_Car.Cars
    add constraint rent_car_cars_foreign_key
        foreign key (Owner_ID)
            references Rent_Car.Users (id_users)
;

alter table Rent_Car.Trip
    add constraint rent_car_trip_foreign_key
        foreign key (Driver_ID)
            references Rent_Car.Users (id_users),
    add constraint rent_car_cars_foreign_key
        foreign key (Machine_ID)
            references Rent_Car.Cars (id_cars)
;

--drop schema if exists Rent_Car cascade;









